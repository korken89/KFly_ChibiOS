# List of all the module's related files.
ESTIMATION_SRCS = $(MODULE_DIR)/estimation/src/attitude_ekf.c \
                  $(MODULE_DIR)/estimation/src/estimation.c \
                  $(MODULE_DIR)/estimation/src/vicon_estimator.c

# Required include directories
ESTIMATION_INC = $(MODULE_DIR)/estimation/inc
